#!/usr/bin/env bash

set -euxo pipefail

# Install dependencies
apt-get update -y
apt-get install -y software-properties-common

# Install repos
add-apt-repository -y ppa:jcfp/nobetas
add-apt-repository -y ppa:jcfp/sab-addons

apt-get install -y sabnzbdplus 7zip par2-turbo

# Uninstall unneeded stuff
apt-get remove -y software-properties-common
apt-get autoremove -y
/setup/common/clean.sh

sabnzbdplus -v
