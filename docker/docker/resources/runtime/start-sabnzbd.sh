#!/usr/bin/env bash

set -eu

echo ===================
echo Starting Sabnzbd ...
echo ===================

DATA_DIR=${DATA_DIR:?Data dir not set}
DOWNLOAD_DIR=${DOWNLOAD_DIR:?Download dir not set}
COMPLETE_DIR=${COMPLETE_DIR:?Complete dir not set}
API_KEY=${API_KEY:-}

CONFIG_FILE=${DATA_DIR}/sabnzbd.ini

# $1: JSON key
# $2: value
replace_line_in_file() {
  if [[ -n "${2}" ]]; then
    echo Replacing \""${1}"\" in config: \""${2}"\"
    sed -i "${CONFIG_FILE}" \
      -e "s#${1} = .*#${1} = ${2}#"
  fi
}

mkdir -p "$DATA_DIR"

if [[ ! -f ${CONFIG_FILE} ]]; then
  echo "Config does not exist, copying template..."
  cp -prv --update=none /runtime/config/* "$DATA_DIR"

  if [[ -z "${API_KEY}" ]]; then
    API_KEY=$(tr -dc 'a-z0-9' </dev/urandom | fold -w 32 | head -n 1)
    echo Created new API key: "${API_KEY}"
  fi
fi

### replace values in config
if [[ -n "$API_KEY" ]]; then
  replace_line_in_file "api_key" "$API_KEY"
fi
replace_line_in_file "download_dir" "$DOWNLOAD_DIR"
replace_line_in_file "complete_dir" "$COMPLETE_DIR"
replace_line_in_file "host_whitelist" "$HOST_WHITELIST"

### start
sabnzbdplus -s 0.0.0.0:8080 --repair -b0 -f "$CONFIG_FILE"
