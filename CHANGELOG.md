# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- ini config file

### Changed
- Update sabnzbd to 2.3.7


## [v2.3.1]
### Added
- First release, contains sabnzbd 2.3.5

[v2.3.1]: ../../src/v2.3.1
[Unreleased]: ../../src
