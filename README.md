# docker-sabnzbd

[![](https://img.shields.io/bitbucket/pipelines/mreil-com/docker-sabnzbd.svg)](https://bitbucket.org/mreil-com/docker-sabnzbd/addon/pipelines/home)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

> [Sabnzbd](https://sabnzbd.org/) image

Based on [mreil/ubuntu-base][ubuntu-base].

## Usage

    docker run -i -t \
    -p 8080:8080 \
    --mount VOLUME1:/mnt/sab-data \
    --mount VOLUME2:/mnt/sab-incomplete \
    --mount VOLUME3:/mnt/sab-complete \
    mreil/sabnzbd

### Environment variables

| Variable       | Description                                                | Default             |
|----------------|------------------------------------------------------------|---------------------|
| **API_KEY**    | Sets the API KEY which is required to access the REST API. | Random              |
| DATA_DIR       | Where SABnzbd keeps its data and config file.              | /mnt/sab-data       |
| DOWNLOAD_DIR   | Incomplete files go here.                                  | /mnt/sab-incomplete |
| COMPLETE_DIR   | Completely downloaded files go here.                       | /mnt/sab-complete   |
| HOST_WHITELIST | Hostnames that are allowed to access UI and API            | sabnzbd             |

* :

#### Configuration

A connection to NEWS.NEWSHOSTING.COM is preconfigured.
You just need to update the username/password in the settings GUI.

## Build

    ./gradlew build

## Releases

See [hub.docker.com][docker-hub-releases].

## License

See [LICENSE](LICENSE).


[ubuntu-base]: https://bitbucket.org/mreil-com/docker-ubuntu-base

[docker-hub-releases]: https://hub.docker.com/r/mreil/sabnzbd/tags/
